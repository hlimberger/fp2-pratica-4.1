/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Henrique Kolling Limberger
 */
public class Elipse {
    private final double eixoY;
    private final double eixoX;
    
    public Elipse(double semiEixoY, double semiEixoX){
        this.eixoY = semiEixoY * 2;
        this.eixoX = semiEixoX * 2;
    }
    
    public double getArea(){
        double x = eixoX / 2;
        double y = eixoY / 2;
        return Math.PI * x * y;
    }
    
    public double getPerimetro(){
        double x = eixoX / 2;
        double y = eixoY / 2;
        return Math.PI * ((3 * (x + y)) - Math.sqrt(((3 * x) + y) * (x + (3 * y))));
    }
    protected double getEixoY(){
        return eixoY;
    }
    protected double getEixoX(){
        return eixoX;
    }
}
